

/**
 * Created by Ruslan on 21.08.2017.
 */
public class TaskCh11N245 {
    static void duplicates(int[] arr)
    {
        int len = arr.length;
        int start = 0;
        int newArr[] = new int[len];

        for (int i =0; i<arr.length; i++) {
                if (arr[i] < 0) {
                    newArr[start]=arr[i];
                    start++;
                } else {newArr[len-1] = arr[i]; len--;}
            }
        for (int i = 0; i<arr.length; i++){
            System.out.print(newArr[i]);
        }
    }

    public static void main(String[] args) {
        duplicates(new int[] {1,-2,3,-1,-2});
    }
}
