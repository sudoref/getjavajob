import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Ruslan on 20.08.2017.
 */
public class TaskCh10N055 {
    private static char map[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int i;
        int num ;

        do {
            System.out.println("Insert number ");
             num = Integer.parseInt(reader.readLine());
            System.out.println("Insert base");
            i = Integer.parseInt(reader.readLine());
        } while (i<0 || i >16);

        System.out.println(converter(num, i, ""));
    }
    public static String converter(int num, int i, String result){
        int rod = num%i;
        int div = num/i;

        if (div < 0){
            return converter(div, i, map[rod]+result);
        } else return map[rod]+result;
    }
}
