/**
 * Created by Ruslan on 19.08.2017.
 */
public class TaskCh10N048 {

    static int maxElem(int[] arr, int start) {
        if (isMaxElement(arr, arr[start])){
            return arr[start];
        }else {
            return maxElem(arr, start + 1);
        }
    }


    static boolean isMaxElement(int[] arr, int element) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > element)
                return false;
        }
        return true;
    }


    public static void main(String[] args) {
        int[]arr ={1,10,200,9,44};
        System.out.println(maxElem(arr, 0));
    }
}
