import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by Ruslan on 22.08.2017.
 */
class Worker  {
    String name;
    String address;
    int month;
    int year;
    Worker(String name, String address, int month, int year ){
        this.address = address;
        this.month = month;
        this.year = year;
        this.name = name;
    }
}
public class TaskCh13N012 {

    public static void main(String[] args) {
        int month = 8;
        int year = 2017;
        ArrayList<Worker> workers= new ArrayList<>();

        Worker worker1 = new Worker("Иванов Иван", "Saint-petersburg", 12,2014);
        Worker worker2 = new Worker("Дмитриев Дмитрий", "Saint-petersburg", 9,2001);
        Worker worker3 = new Worker("Петров Петр", "Saint-petersburg", 7,2012);
        Worker worker4 = new Worker("Иванов Петр", "Saint-petersburg", 7,2015);
        Worker worker5 = new Worker("Петров Иван", "Saint-petersburg", 8,2017);
        Worker worker6 = new Worker("Петров Дмитрий", "Saint-petersburg", 1,2008);
        Worker worker7 = new Worker("Дмитриев Петр", "Saint-petersburg", 7,2012);
        Worker worker8 = new Worker("Иванов Дмитрий", "Saint-petersburg", 8,2016);
        Worker worker9 = new Worker("Дмитриев Иван", "Saint-petersburg", 7,2012);
        Worker worker10 = new Worker("Петров Петр", "Saint-petersburg", 7,2011);

        workers.add(worker1);
        workers.add(worker2);
        workers.add(worker3);
        workers.add(worker4);
        workers.add(worker5);
        workers.add(worker6);
        workers.add(worker7);
        workers.add(worker8);
        workers.add(worker9);
        workers.add(worker10);



        for (Worker worker : workers){
            if (year - worker.year >3 ){
                System.out.println(worker.name + " " + worker.address );
            } else if (year - worker.year == 3){
                if (month < worker.month ){
                    System.out.println(worker.name + " " + worker.address );
                }
            }
        }
    }

}
