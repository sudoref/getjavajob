import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Ruslan on 20.08.2017.
 */
public class TaskCh10N056 {



    public static boolean recursion(int n, int i) {

        if (n < 2) {
            return false;
        }
        else if (n == 2) {
            return true;
        }
        else if (n % i == 0) {
            return false;
        }
        else if (i < n / 2) {
            return recursion(n, i + 1);
        } else {
            return true;
        }
    }
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("insert the number");
        int n = Integer.parseInt(reader.readLine());

        System.out.println("number is prime? "+recursion(n, 2)); // вызов рекурсивной функции
    }
}

