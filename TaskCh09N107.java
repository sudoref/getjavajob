/**
 * Created by Ruslan on 16.08.2017.
 */
public class TaskCh09N107 {
    public static void main(String[] args) {
        String str = "argon";
        char[] st = str.toCharArray();
        int k = 0;
        int n = 0 ;
        char t;
        if (str.contains("o") && str.contains("a")) {
            for (int i = 0; i < st.length; i++) {
                if (st[i] == 'a') {
                    k = i;
                    break;
                }
            }
            for (int j = st.length - 1; j >= 0; j--) {
                if (st[j] == 'o') {
                    n = j;
                    break;
                }
            }

            t = st[k];
            st[k] = st[n];
            st[n] = t;
            System.out.println(st);
        } else System.out.println("There is no symbol a or o");



    }
}
