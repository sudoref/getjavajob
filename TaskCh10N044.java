/**
 * Created by Ruslan on 18.08.2017.
 */
public class TaskCh10N044 {

    static int dig(int n)
    {
        if (n == 0) return n;
        else {
            return n % 10 + dig(n / 10);
        }
    }

    static int root(int n)
    {
        int result=dig(n);
        if (result/10 == 0){
            return result;
        } else {
            return root(result);
        }
    }



    public static void main(String[] args) {
        System.out.println(root(888));

    }
}
