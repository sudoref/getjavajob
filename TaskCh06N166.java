/**
 * Created by Ruslan on 16.08.2017.
 */
public class TaskCh06N166 {
    public static void main(String[] args) {
        String tmp;
        String str = "Today is a great day";
        String[] words = str.split(" ");

        tmp = words[0];
        words[0] = words[words.length-1];
        words[words.length-1] = tmp;


        for (String word : words) {
            System.out.print(word + " ");
        }
    }
}
