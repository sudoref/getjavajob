/**
 * Created by Ruslan on 18.08.2017.
 */
public class TaskCh10N043 {
    static int i =0;
    static int k = 0;
    static int sum(int n){
        if (n != 0){
            i += n%10;
            k++;
            return sum(n/10);

        } else {
            return i;
        }

    }

    public static void main(String[] args) {
        System.out.println("Sum of all numbers = "+ sum(4987 )+" " + "Number of symbols "+ k);
    }
}
