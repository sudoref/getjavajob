import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ruslan on 28.08.2017.
 */
interface Human {
     default void talks(){
         System.out.println("Default");
     } //POLYMORPHISM. It's a new feature of Java8 interface with default method


}
class Employer implements Human{
    private String word = "hi! introduce yourself and describe your java experience please";
    public void talks() {
        System.out.println(this.word);
    }
}
abstract class Candidate implements Human{

    String name; //ENCAPSULATION. Name is private and cant be reach from others classes

    void Candidate(String name){ //Default constructor
        this.name = name;
    }
    abstract void introduce (); // Abstract method


}
class GraduatedCandidate extends Candidate{ //INHERITANCE.
    GraduatedCandidate(String name){
        Candidate(name);
    }

    @Override
    void introduce() {
        System.out.println("hi! may name is "+ this.name  +" !");
    }

    private String sentence = "I passed successfully getJavaJob exams and code reviews.";
    public void talks() {
        System.out.println(sentence);
    }
}
class SelfLearnerCandidate extends  Candidate{ //INHERITANCE.
    void introduce() {
        System.out.println("hi! may name is "+ this.name  +" !");
    }
    SelfLearnerCandidate(String name){ //defalut constructor
        Candidate(name);
    }
    private String sentence = "I have been learning Java by myself, nobody examined how thorough is my knowledge and how good is my code.";
    public void talks() {
        System.out.println(sentence);
    }
}

class Interview {

    public static void main(String[] args) {

        Employer emp = new Employer();
        List<GraduatedCandidate> candidateList = new ArrayList<>();
        List<SelfLearnerCandidate> candidateList1 = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            GraduatedCandidate candidate = new GraduatedCandidate("candidate" + i); //creating list of Graduetedcandidates
            candidateList.add(candidate);

        }
        for (int i = 0; i < 5; i++) {
            SelfLearnerCandidate candidate = new SelfLearnerCandidate("candidate" + (i+5)); //creating list of SelfLearnerCandidates
            candidateList1.add(candidate);

        }

        for (GraduatedCandidate candidate:candidateList // call of methods
             ) {
            System.out.println("employer is asking: ");
            emp.talks();
            System.out.println();
            System.out.println("candidate is responding: ");
            candidate.introduce();
            candidate.talks();
            System.out.println();
        }
        for(SelfLearnerCandidate candidate: candidateList1){ // call of methods
            System.out.println("employer is asking: ");
            emp.talks();
            System.out.println();
            System.out.println("candidate is responding: ");
            candidate.introduce();
            candidate.talks();
            System.out.println();
        }

    }

}