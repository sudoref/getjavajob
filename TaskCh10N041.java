import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Ruslan on 18.08.2017.
 */
public class TaskCh10N041 {
    public static int fact(int n) {
        if(n == 1){
            return 1;
        }
        return n*fact(n-1);
    }
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());
        System.out.println(fact(n));
    }
}
