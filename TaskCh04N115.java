import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Ruslan on 14.08.2017.
 */
public class TaskCh04N115 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n;
        do {
            System.out.println("Insert the year");
            n = Integer.parseInt(reader.readLine());
        } while (n < 1984);


        int colorNum = (n - 1984)%10;
        int yearNum = (n - 1984)%12;
        String beast;
        String color;
        switch (colorNum){
            case 0:
            case 1:
                color = "green";
                break;
            case 2:
            case 3:
                color = "red";
                break;
            case 4:
            case 5:
                color = "yellow";
                break;
            case 6:
            case 7:
                color = "white";
                break;
            case 8:
            case 9:
                color = "blue";
                break;
                default:
                    color = "color";

        } switch (yearNum) {
            case 0:
                beast = "rat";
                break;
            case 1:
                beast = "bull";
                break;
            case 2:
                beast = "tiger";
                break;
            case 3:
                beast = "rabbit";
                break;
            case 4:
                beast = "dragon";
                break;
            case 5:
                beast = "snake";
                break;
            case 6:
                beast = "horse";
                break;
            case 7:
                beast = "sheep";
                break;
            case 8:
                beast = "monkey";
                break;
            case 9:
                beast = "cock";
                break;
            case 10:
                beast = "dog";
                break;
            case 11:
                beast = "pig";
                break;
                default:
                    beast = "beast";
        }
        System.out.println(color + " " + beast);

    }
}
