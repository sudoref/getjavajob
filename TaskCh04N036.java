import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by Ruslan on 14.08.2017.
 */
public class TaskCh04N036 {
    public static void main(String[] args) throws Exception{
        System.out.println("insert minute");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int i = Integer.parseInt(reader.readLine());

        if (i%5 > 2){
            System.out.println("red");
        } else System.out.println("green");
    }
}
