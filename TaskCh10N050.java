/**
 * Created by Ruslan on 19.08.2017.
 */
public class TaskCh10N050 {
    static int akker(int n, int m){
        if (n == 0){
            return m+1;
        } else if (n!=0 && m ==0){
            return akker(n-1,1);
        } else {
            return akker(n-1, akker(n,m-1));
        }
    }
    public static void main(String[] args) {
        System.out.println(akker(1,3));
    }
}
