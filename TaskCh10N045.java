import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Ruslan on 18.08.2017.
 */
public class TaskCh10N045 {
    static int step;
    static int n;

    static int progression(int i){
        if (i == 0){
            return n;
        } else {
            n += step;

            return progression(i -1);
        }

    }
    static int sum ( int sum, int step,int i){
        if (i !=0) sum+=(i*step)+sum(sum, step,i-1);
        return sum;
    }
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Insert 1 number of progression");
        n = Integer.parseInt(reader.readLine());
        System.out.println("Insert the step of progression");
        step = Integer.parseInt(reader.readLine());
        System.out.println("Insert the final point of progression");
        int i = Integer.parseInt(reader.readLine());

        System.out.println(progression(i));
        System.out.println(sum(n,step,i));
    }
}
