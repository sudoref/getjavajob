/**
 * Created by Ruslan on 15.08.2017.
 */
public class TaskCh05N064 {
    public static void main(String[] args) {
        int []s = new int[12]; // land area for each district
        int []p = new int[12]; // people in each district

        int sres =0;
        int pres = 0;
        for (int i = 0; i < s.length; i++){
            s[i] = (i+1)*100;
            sres+= s[i];
            p[i] = (i+1)*1000;
            pres += p[i];

        }
        System.out.println("population density " + pres/sres + " men per 1 km");

    }
}
