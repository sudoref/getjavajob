/**
 * Created by Ruslan on 14.08.2017.
 */
public class TaskCh05N038 {
    public static void main(String[] args) {
        double N = 1;
        double result =0;
        double k =0;


        for (int i = 2; i < 100; i++){
            result += N/i;
            if (i%2==0){
                k += N/i;
            } else k-=N/i;
        }
        System.out.println(result);
        System.out.println(k);
    }
}
