import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by Ruslan on 11.08.2017.
 */
public class Gjj239 {
    public static void main(String[] args) throws Exception {
        System.out.println("введите время");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int h = Integer.parseInt(reader.readLine());
        int m = Integer.parseInt(reader.readLine());
        int s = Integer.parseInt(reader.readLine());

        if( h<=0 || h>=24 || m<=0 || m>=60 || s<=0 || s>=60 ){
            if (h >= 12){
                h = h - 12;
            }
            double angle = (h*30.0 + m/2.0 + s/120); // использую формулу для вычисления угла
            if (angle > 180)
                angle = 360 - angle;
            System.out.println(angle + " градусов");
        }
        else {
            System.out.println("Введен неверный формат времени");
        }

    }
}
