import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Ruslan on 14.08.2017.
 */
public class TaskCh04N067 {
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int k;

        do {
            System.out.println("insert the day");
            k = Integer.parseInt(reader.readLine());
        } while ( k < 0 || k > 365 );

        if ((k%7)>5 || (k%7)<1){
            System.out.println("Weekend");
        } else System.out.println("Workday");


    }
}
