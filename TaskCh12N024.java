/**
 * Created by Ruslan on 21.08.2017.
 */
public class TaskCh12N024 {
    public static void main(String[] args) {
        int x = 6;
        int arr[][] = new int[x][x];
        int a =0;
        for (int i = 0; i < x; i++) {
            arr[i][a]=1;
            for (int j = 0; j < x; j++) {
                    arr[a][j]=1;
                if (i>0&&j>0){
                    arr[i][j] = arr[i-1][j]+arr[i][j-1];
                }
            }
        }
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < x; j++) {
                System.out.print(arr[i][j]);
            }
            System.out.println();
        }
        System.out.println();

        int arr2[][] = new int[x][x];
        int b =0;
        for (int i = 0; i < x; i++) {

            for (int j = 1; j < x; j++) {
                arr2[b][j]=j+1;

                    arr2[i][j] = (((i+1) + (j+1) - 2) % x) + 1; // required arithmetic expression

            }
            arr2[i][b]=i+1;

        }

        for (int i = 0; i < x; i++) {
            for (int j = 0; j < x; j++) {
                System.out.print(arr2[i][j]);
            }
            System.out.println();
        }

    }
}
