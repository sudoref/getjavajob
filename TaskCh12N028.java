/**
 * Created by Ruslan on 21.08.2017.
 */
public class TaskCh12N028 {
    public static void main(String[] args) {
        int n = 7;
        int[][] arr = new int[n][n];

        int x = 0;
        int y = 0;
        int dx = 1;
        int dy = 0;
        int direction = 0;
        int v = n;

        for (int i = 0; i < n * n; i++) {
            arr[x][y] = i + 1;
            if (--v == 0) {
                v = n * (direction % 2) + n * ((direction + 1) % 2) - (direction / 2 - 1) - 2;
                int temp = dx;
                dx = -dy;
                dy = temp;
                direction++;
            }
            x += dx;
            y += dy;
        }

        for (int j = 0; j < n; j++) {
            for (int i = 0; i < n; i++)
                System.out.print(arr[i][j] + " ");
            System.out.println();
        }
    }
}

