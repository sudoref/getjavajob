import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Ruslan on 14.08.2017.
 */
public class TaskCh04N033 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("insert number");
        int i = Integer.parseInt(reader.readLine());

        if ((i%10)%2==0){
            System.out.println("Last number is even");
        } else System.out.println("Last number is odd");

    }
}
