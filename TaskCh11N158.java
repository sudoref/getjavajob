
import java.util.Arrays;

/**
 * Created by Ruslan on 20.08.2017.
 */
public class TaskCh11N158 {
    static void duplicates(int[] arr)
    {
        int len = arr.length;

        for (int i =0; i<len; i++){
            for(int j = i+1; j<len;j++){
                if (arr[i] == arr[j]){  // проверка на совпадение
                    arr[j] = arr[len-1]; // если совпадение найдено, то совпадению присваивается значение из конца
                    len--; // уменьшаем длинну

                    j--; // возвращаемся на шаг назад, чтобы снова проверить массив от заданной точки
                }
            }
        }
        int[] newArr = Arrays.copyOf(arr, len);
        for (int i = 0; i<newArr.length; i++){
            System.out.print(newArr[i]);
        }
    }
    public static void main(String[] args) {
        duplicates(new int[]{1,2,3,4,4,5,5,6,7,8,9,9,9});
    }
}
