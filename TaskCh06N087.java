import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Ruslan on 15.08.2017.
 */
public class TaskCh06N087 {
    public static void main(String[] args) throws IOException {
        Game game = new Game();
        game.play();

    }
}
class Game {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    String teamName1;
    String teamName2;
    int score;
    int score1;
    int score2;

    public void play() throws IOException {

        System.out.println("Enter team #1: ");
        teamName1 = reader.readLine();
        System.out.println("Enter team #2: ");
        teamName2 = reader.readLine();



        while (true)
        {
            System.out.println("Enter team to score (1 or 2 or 0 to finish game)");
            int opt = Integer.parseInt(reader.readLine());
            if (opt == 1){
                System.out.println("Enter the score");
                score = Integer.parseInt(reader.readLine());
                if (score >0 && score < 4) {
                    score1 += score;
                    System.out.println(score());
                } else System.out.println("Wrong number");
            }
            else if (opt == 2){
                System.out.println("Enter the score");
                score = Integer.parseInt(reader.readLine());
                if (score >0 && score < 4) {
                    score2 += score;
                    System.out.println(score());
                } else System.out.println("wrong number");
            }
            else if (opt == 0) {
                System.out.println(result());
                break;
            } else System.out.println("wrong number");
        }


    }

    public String score() {
        return (" Team " + teamName1 + " " + score1 + " || " + " Team " + teamName2 + " " + score2);
    }
    String result() {
        if (score1 > score2) {
            return ("Team "+ teamName1 + " won with count "+ score1 + ":" + score2);
        } else if (score1 < score2) {
            return ("Team "+ teamName2 + " won with count "+ score1 + ":" + score2);
        } else if (score1 == score2){
            return ("DRAW" + score1 + ":" + score2);
        }
        return "";
    }

}
