/**
 * Created by Ruslan on 19.08.2017.
 */
public class TaskCh10N049 {

    static int index(int[] arr, int start) {
        if (isMaxElement(arr, arr[start]))
            return start;
        return index(arr, start + 1);
    }


    static boolean isMaxElement(int[] arr, int element) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > element)
                return false;
        }
        return true;
    }



    public static void main(String[] args) {
        int[]arr ={1,10,92,44,101};

        System.out.println( index(arr, 0));
    }
}
