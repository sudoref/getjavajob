import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Ruslan on 18.08.2017.
 */

public class TaskCh10N053  {
    static void sort(int[]i, int a)   {
        if (i[a]<i.length){
            sort(i,a+1);
            System.out.print(i[a]);
        } else System.out.print(i[a]);
    }
    static void sort(int[]i){
        sort(i, 0);
    }
    public static void main(String[] args) throws IOException  {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[]arr =  new int[5];
        for (int i =0; i<arr.length; i++){
            arr[i] = Integer.parseInt(reader.readLine());
        }
        sort(arr);
    }

}