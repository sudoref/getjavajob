import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Ruslan on 15.08.2017.
 */
public class TaskCh06N008 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());

        double [] arr = new double[10];

        for (int i = 0; i < arr.length; i++){
            arr[i] = Math.pow(i+1, 2);
            while (arr[i]<n) {
                System.out.println(arr[i]);
                if (n > arr[i]){
                    break;
                }
            }
        }
    }
}
