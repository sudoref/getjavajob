import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Ruslan on 18.08.2017.
 */
public class TaskCh10N042 {

        public static int pow(int i, int n) {

            if(n == 1){
                return i ;
            } else {

                return  i*pow(i,n - 1);

            }

        }
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int i = Integer.parseInt(reader.readLine());
        int n = Integer.parseInt(reader.readLine());
        System.out.println(pow(i,n));
    }


}
