import com.sun.javafx.image.IntPixelGetter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Ruslan on 14.08.2017.
 */
public class TaskCh04N015 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int birthMounth;
        int birthYear;
        int todayMounth;
        int todayYear;

        int result;

        do {
            System.out.println("Insert birth date");

            birthMounth = Integer.parseInt(reader.readLine());
            birthYear = Integer.parseInt(reader.readLine());

        } while ((birthMounth > 1 || birthMounth < 12) && (birthYear > 1800 || birthYear < 2017));

        do {
            System.out.println("Insert today date");

            todayMounth = Integer.parseInt(reader.readLine());
            todayYear = Integer.parseInt(reader.readLine());
        }   while ((todayMounth < 1 || todayMounth > 12) && todayYear < birthYear );

        if (todayMounth >= birthMounth ){
            System.out.println(todayYear - birthYear);
        } else {
            System.out.println(todayYear - birthYear - 1);
        }
    }
}
