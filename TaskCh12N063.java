import java.util.Random;

/**
 * Created by Ruslan on 22.08.2017.
 */
public class TaskCh12N063 {
    public static void main(String[] args) {
        double arr[][] = new double[11][4];
        Random r = new Random();
        int s = r.nextInt(30);
        int sum=0;

        for (int i =0; i< 11; i++){
            for (int j =0; j<4; j++){
                arr[i][j] = s%11+10;

            }
        }
        for (int i = 0; i < 11; i++){
            sum = 0;
            for(int j = 0; j< 4; j++){
                sum+=arr[i][j];
                sum = sum/4;

                System.out.println( i+1 + "классов "+ sum + "учеников" );
            }
        }
    }
}
