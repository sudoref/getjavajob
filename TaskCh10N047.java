/**
 * Created by Ruslan on 19.08.2017.
 */
public class TaskCh10N047 {
    static int fibo(int n){
        if (n<2){
            return n;
        } else return (fibo(n-1)+fibo(n-2));
    }
    public static void main(String[] args) {
        System.out.println(fibo(5));
    }
}
